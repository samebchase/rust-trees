pub enum Tree<T> {
    Node (Box<Tree<T>>, T, u8, Box<Tree<T>>),
    Nil
}

impl<T> Tree<T> {
    
    pub fn new<T>() -> Tree<T> {
        Nil
    }

    pub fn get_left_child(self) -> Box<Tree<T>> {
        match self {
            Node (left_child_ptr, data, balance_factor, right_child_ptr) => left_child_ptr,
            Nil => box Nil
        }
    }

    pub fn get_right_child(&self) -> Tree<T> {
        Nil
    }
    
    // pub fn get_data<T>(&self) -> T {
    // }

    pub fn get_balance_factor(&self) -> u8 {
        1
    }

    pub fn set_left_child(& mut self) -> () {
    }

    pub fn set_right_child(& mut self) -> () {
    }

    pub fn set_balance_factor(& mut self) -> () {
    }

    pub fn insert<T>(& mut self, insertee: T) -> Tree<T> {
        Nil
    }

    pub fn delete<T>(& mut self, deletee: T) -> Tree<T> {
        Nil
    }

    pub fn size<T>(&self) -> u64 {
        64
    }
}