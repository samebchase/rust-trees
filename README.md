### An attempt to implement an AVL Tree in Rust

#### Building and running the tests

You'll need to install [Cargo](https://github.com/rust-lang/cargo) first.

```
$ cargo build
$ cargo test
```
